#include "rmsScheduler.h"
#include "math.h"


rmsScheduler::rmsScheduler(int quantum) : scheduler(quantum)
{
	cout << "rms scheduler generated." << endl;
}



int rmsScheduler::choose_next_task()
{
	try {
		// get the lowest period
		int temp_period = all_tasks[0]->period;
		int temp_id = all_tasks[0]->id;
		for (int i = 1; i < all_tasks.size(); i++)
		{
			temp_period = ((all_tasks[i]->period) < (temp_period)) ? all_tasks[i]->period : temp_period;
			temp_id = ((all_tasks[i]->period) == (temp_period)) ? all_tasks[i]->id : temp_id;
		}
		return temp_id;
	}
	catch (exception e) { return -1; }

}





