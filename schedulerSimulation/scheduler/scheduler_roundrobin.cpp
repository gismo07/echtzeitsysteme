#include "scheduler_roundrobin.h"


scheduler_roundrobin::scheduler_roundrobin(int quantum) : scheduler(quantum)
{
	// start with first task
	currTaskID = -1;
    cout << "roundrobin scheduler generated." << endl;
}



int scheduler_roundrobin::choose_next_task()
{
  // how much tasks are their still?
  int N = all_tasks.size();


  // select next task
  int nextTaskID = (currTaskID + 1)%N;

  // get that task
  task* t = all_tasks[nextTaskID];

  currTaskID = nextTaskID;

  // return the id of that task
  return t->id;
}
