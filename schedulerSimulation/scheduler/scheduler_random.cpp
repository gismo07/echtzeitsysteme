#include "scheduler_random.h"


scheduler_random::scheduler_random(int quantum) : scheduler(quantum)
{
  cout << "random scheduler generated." << endl;
}



int scheduler_random::choose_next_task()
{
  // how much tasks are their still?
  int N = all_tasks.size();

  // select randomly a task vector list index
  int rnd_id = rand() % N;

  // get that task
  task* t = all_tasks[rnd_id];

  // return the id of that task
  return t->id;  
}



