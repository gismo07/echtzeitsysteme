#include <conio.h>
#include <iostream>

#include "scheduler_random.h"
#include "scheduler_roundrobin.h"
#include "rmsScheduler.h"

using namespace std;

int main()
{
	// define quantum
	int quantum = 10;

	bool isRandomScheduler = true;

	bool deadlineViolation = false;

	//create scheduler
	//scheduler_roundrobin s(quantum);
	//scheduler_random s(quantum);
	rmsScheduler s(quantum);

	/*
	// generate 5 tasks with random length of needed computation times
	const int N = 5;
	for (int i = 0; i < N; i++)
	{
	s.add_new_task((rand() % 5 + 1) * 100); // time is 100,200,300,400,or 500 msec
	}
	*/

	// let user define N tasks
	int n;
	cout << "Please enter number of tasks to simulate: " << ends;
	cin >> n;
	const int N = n;
	deque<task*> all_tasks;

	for (int i = 0; i < N; i++) {
		int computationTime;
		int period;

		// Let user define this single task
		cout << "Enter computation time for task " << i << ends;
		cin >> computationTime;
		cout << "Enter period time for task " << i << ends;
		cin >> period;

		// Add this task
		s.add_new_task(i, computationTime, period);


		// store all tasks to generate them later...
		task* t = new task;
		t->id = i;
		t->time_needed = computationTime;
		t->period = period;
		t->time_computed = 0;
		all_tasks.push_back(t);
	}


	// create array to store how many instances have been started allready
	int* instancesStarted = new int[N];
	for (int i = 0; i < N; i++) {
		// init array
		instancesStarted[i] = 1;
	}


	// check schedulablility
	s.checkSchedulability();
	_getch();

	// simulate until we are finished
	int simulation_time = 0;
	while (!deadlineViolation)
	{
		cout << "\nSimulation time : " << simulation_time << endl;
		cout << "Quantum = " << quantum << endl;



		/////////////////////////////////////////////////////////////////
		// generate tasks periodicaly
		for (int i = 0; i < N; i++)
		{
			task* t = all_tasks[i];
			if (instancesStarted[i] < ((simulation_time / t->period)) + 1) {

				deadlineViolation = (s.get_task_with_id(t->id)!=NULL);

				// check for deadline violation
				s.add_new_task(t->id, t->time_needed, t->period);

				// update instancesStarted array
				instancesStarted[t->id]++;
			}
		}
		/////////////////////////////////////////////////////////////////
		int computation_time = quantum;

		if (s.all_tasks_finished())
		{
			cout << "No task to schedule!" << endl;
		}
		else
		{


			/////////////////////////////////////////////////////////////////
			// ask the scheduler to choose a task that shall be computed next
			int task_id = s.choose_next_task();
			/////////////////////////////////////////////////////////////////


			// get that task
			task* t = s.get_task_with_id(task_id);

			// how much time has this task still to compute?
			int computation_time_left = t->time_needed - t->time_computed;

			// is the rest computation < quantum?			
			if (computation_time_left < quantum)
				computation_time = computation_time_left;

			// update time computed so far for this task
			t->time_computed += computation_time;



			// output our decision:
			cout << "Scheduler choosed task with id=" << t->id << endl;

			// show status of all tasks
			s.show_status();

			// is the chosen task finished?
			if (t->time_computed == t->time_needed)
			{
				cout << "Task with id=" << t->id << " is finished with its computation!" << endl;

				// erase task from task vector/list
				s.delete_task(t);
			}
		}


		



		// time goes by ...
		simulation_time += computation_time;



		_getch();
	}
	cout << "\n!!! DEADLINEVIOLATION !!!\n" << endl;
	/*cout << "\n\nAll tasks finished their computation." << endl;*/
	cout << "Simulation finished.Press key to exit." << endl;
	_getch();

} // main
