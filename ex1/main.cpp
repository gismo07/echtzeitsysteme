#include <iostream>
#include <time.h>
#include <algorithm>

using namespace std;

const int N = 1000;
const int randomIterations = 100;
int numbers[N];
double timeSpendBestCase, timeSpendWorstCase;
double timeSpendRandom[randomIterations];

bool generateRandomNumbers() {
    for (int i = 0; i < N; i++) {
        numbers[i] = rand() % 1000;
    }
    return true;
}

void generarteSortedArray(bool ASC) {
    for (int i = 0; i < N; i++) {
        numbers[i] = ASC ? i : N - (i + 1);
    }
}

void bubbleSort() {
    bool ready;
    do {
        ready = true;
        for (int i = 0; i < N - 1; i++) {
            if (numbers[i] < numbers[i + 1]) {
                int tmp = numbers[i];
                numbers[i] = numbers[i + 1];
                numbers[i + 1] = tmp;
                ready = false;
            }
        }
    }
    while (!ready);
}

double runSortAndReturnTimeSpend() {
    clock_t begin = clock();
    bubbleSort();
    clock_t end = clock();
    return (double) (end - begin) / CLOCKS_PER_SEC;
}

void runRandomTestAndStoreTimeSpend()
{
    for(int i =0;i<randomIterations;i++)
    {
        if(generateRandomNumbers())timeSpendRandom[i]=runSortAndReturnTimeSpend();
    }

    sort(timeSpendRandom,timeSpendRandom+randomIterations);
}

double getShortestRandomTimeSpend()
{
    return timeSpendRandom[0];
}

double getLongestRandomTimeSpend()
{
    return timeSpendRandom[randomIterations-1];
}



int main() {

    cout << "\n\nStarting best and worst case test\n" << endl;
    generarteSortedArray(false);
    timeSpendBestCase=runSortAndReturnTimeSpend();

    generarteSortedArray(true);
    timeSpendWorstCase = runSortAndReturnTimeSpend();
    cout << "\tBest case: "<< timeSpendBestCase <<endl;
    cout << "\tWorst case: "<< timeSpendWorstCase <<endl;

    cout << "\n\nStarting random test\n" << endl;
    runRandomTestAndStoreTimeSpend();

    double shortest = getShortestRandomTimeSpend();
    double longest = getLongestRandomTimeSpend();

    //cout << "\tShortest time spend: "<< shortest <<endl;
    //cout << "\tLongest time spend: "<< longest <<endl;

    printf("\tShortest time spend: %f\n",shortest);
    printf("\tLongest time spend: %f\n",longest);




    return 0;
}