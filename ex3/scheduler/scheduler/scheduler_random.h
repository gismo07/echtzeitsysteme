#pragma once

#include <iostream>

#include "scheduler.h"

using namespace std;

class scheduler_random : public scheduler
{
  public:

              scheduler_random(int quantum);
    
    int       choose_next_task();

};
