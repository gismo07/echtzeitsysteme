#include <iostream>
#include <time.h>
#include <iostream>
#include <fstream>

using namespace std;

const int iterations = 50;
const int maxSize = 20; // in MB!

int memSizes[iterations];
int clocksMalloc[iterations];
int clocksFree[iterations];


void runSimulation();

void writeToFile();

int main() {
    cout << "STARTING SIMULATION" << endl;
    runSimulation();
    cout << "\nSIMULATION FINISHED" << endl;
    return 0;
};


void runSimulation() {
    for (int i = 0; i < iterations; i++) {
        int memorySpace = rand() % maxSize + 1;
        clock_t tMalloc, tFree;
        char *buffer;

        tMalloc = clock();
        buffer = (char *) malloc((size_t) (memorySpace * 1024 * 1024));
        //tMalloc = clock() - tMalloc;

        for (int i = 0; i < memorySpace * 1024 * 1024; i++) {
            buffer[i] = (1 == rand() % 10 + 1) ? 'y' : 'n';
        }
        tMalloc = clock() - tMalloc;

        tFree = clock();
        free(buffer);
        tFree = clock() - tFree;

        memSizes[i] = memorySpace;
        clocksMalloc[i] = tMalloc;
        clocksFree[i] = tFree;
    }
    writeToFile();
}

void writeToFile() {
    ofstream mFile;
    mFile.open("/home/johann/ClionProjects/memoryAllocation/allocRun.txt");
    for (int i = 0; i < iterations; i++) {
        mFile << memSizes[i] << " " << ((float) clocksMalloc[i]) / CLOCKS_PER_SEC << " " <<
        ((float) clocksFree[i]) / CLOCKS_PER_SEC;
        mFile << "\n";
    }
    mFile.close();
}