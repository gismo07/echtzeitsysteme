// MEMORY ALLOCATION

clf;
xgrid;
M = fscanfMat('/home/johann/ClionProjects/memoryAllocation/allocRun.txt');
xtitle('MEMORY ALLOCATION');
xlabel('memory block size [MB]');
ylabel('time [s]');
plot(M(:,1),M(:,2),"b*");
plot(M(:,1),M(:,3),"r*");
