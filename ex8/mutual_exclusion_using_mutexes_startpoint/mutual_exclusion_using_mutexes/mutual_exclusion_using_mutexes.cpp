/// Mutual exclusion example
///
/// Two or more threads work on computing
/// some time consuming function
///
/// The results will be written in a shared
/// memory.
///
/// note: the time consuming work to do here
///       is testing for many numbers whether
///       they are perfect numbers.
///       In number theory, a perfect number is
///       a positive integer that is equal to the
///       sum of its positive divisors excluding
///       the number itself.
/// see
/// https://en.wikipedia.org/wiki/Perfect_number
/// and
/// https://de.wikipedia.org/wiki/Vollkommene_Zahl
///
/// Perfect numbers are rare! The 5 perfect numbers
/// smaller than 50.000.000 are:
/// 6 = 1 + 2 + 3
/// 28 = 1 + 2 + 4 + 7 + 14 = 28
/// 496 = 1 + 2 + 4 + 8 + 16 + 31 + 62 + 124 + 248
/// 8.128 = 1 + 2 + 4 + 8 + 16 + 32 + 64 + 127 + 254 + 508 + 1016 + 2032 + 4064
/// 33.550.336 = ...

#include <iostream> // for std::cout
#include <conio.h>  // for _getch()
#include <thread>   // for std:thread
#include <mutex>    // for std::mutex

using namespace std;

mutex m;

const int max_number_to_test = 50000000;
bool* array_is_perfect_number;
int next_number_to_test = 1;


bool test_for_perfect_number(int number_to_test)
{
  int sum = 0;
  for (int i = 1; i < number_to_test; i++)
  {
    if (number_to_test % i == 0)
      sum += i;
  }

  return (sum == number_to_test);

} // test_for_perfect_number


void tester()
{
  // Lock mutex
  m.lock();

  while(next_number_to_test<=max_number_to_test){
    // Check if perfect number
    array_is_perfect_number[next_number_to_test]=test_for_perfect_number(next_number_to_test);

    // Output 
    if(array_is_perfect_number[next_number_to_test])cout << "Found a perfect number: "<< next_number_to_test << endl;
  
    // increase next_number_to_test
    next_number_to_test++;
  }

  // Unlock mutex
  m.unlock();
}



int main()
{
  // 1. generate result array
  array_is_perfect_number = new bool[max_number_to_test];

  // 2. create some perfect number tester threads
  const int nr_threads = 4;
  std::thread* my_threads[nr_threads];
  for (int i = 0; i < nr_threads; i++)
    my_threads[i] = new thread( tester );

  // 3. wait for all threads to be finished
  cout << "Waiting for all threads to finish ..." << endl;
  for (int i = 0; i < nr_threads; i++)
    my_threads[i]->join();

  // 4. tell the user that we reached the end of the demo
  cout << "End of mutual exclusion example reached." << endl;
  _getch();
}
